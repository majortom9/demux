/* demux -- simple demux tool for the Linux DVB S2 API
 *
 * UDL (updatelee@gmail.com)
 * Derived from work by:
 * 	Igor M. Liplianin (liplianin@me.by)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "demux.h"

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define BUFFY (10*188*1024)
#define MAX_PES_SIZE (4*1024)

static void daemonize(void)
{
    pid_t pid, sid;

    /* already a daemon */
    if ( getppid() == 1 ) return;

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    /* If we got a good PID, then we can exit the parent process. */
    if (pid > 0) {
        printf("PID:        %d \n", pid);
        exit(EXIT_SUCCESS);
    }

    /* At this point we are executing as the child process */

    /* Change the file mode mask */
    umask(0);

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory.  This prevents the current
       directory from being locked; hence not being able to remove it. */
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    /* Redirect standard files to /dev/null */
    if(freopen( "/dev/null", "r", stdin) == 0);
    if(freopen( "/dev/null", "w", stdout) == 0);
    if(freopen( "/dev/null", "w", stderr) == 0);
}

int sendtodecoder(int data_fd, int decoder_fd)
{
/* causing problems 
	if (ioctl(decoder_fd, VIDEO_SELECT_SOURCE, VIDEO_SOURCE_MEMORY) < 0)
		printf("VIDEO_SELECT_SOURCE MEMORY failed (%m)");
	if (ioctl(decoder_fd, VIDEO_SET_STREAMTYPE, 0) < 0)
		printf("VIDEO_SET_STREAMTYPE failed(%m)");
	if (ioctl(decoder_fd, VIDEO_CLEAR_BUFFER) < 0)
		printf("VIDEO_CLEAR_BUFFER: %m");
	if (ioctl(decoder_fd, VIDEO_PLAY) < 0)
		printf("VIDEO_PLAY failed (%m)");
	if (ioctl(decoder_fd, VIDEO_CONTINUE) < 0)
		printf("VIDEO_CONTINUE: %m");

	int len;
	uint8_t buf[MAX_PES_SIZE];
	int written;
	uint64_t length = 0;

  	printf("Outputing demux to decoder ...\n");

 	struct pollfd pfd[1];
	pfd[0].fd = data_fd;
	pfd[0].events = POLLIN;
    int c;
    set_conio_terminal_mode();
    do {
		if (poll(pfd,1,1))
		{
			if (pfd[0].revents & POLLIN)
			{
				len = read(data_fd, buf, sizeof(buf));
				if (len < 0) {
					if (errno == EOVERFLOW)
						fprintf(stderr, "READ ERROR: buffer overflow (%d)\n", EOVERFLOW);
					else
						perror("READ ERROR");
					exit(0);
				}

				written = 0;
				while (written < len) 
					written += write(decoder_fd, buf, len);
				length += len;
//				printf("Written %2.0f MB ...\r", length/1024./1024.);
			}
		}
		if ( kbhit() )
	   		c = kbgetchar(); // consume the character
    } while( (char)c != 'q' );
*/ 
}

int sendtofile(int dvr_fd, FILE * dvr_output)
{
	int len;
	int written;
	int stime = 0;
	uint8_t buf[BUFFY];
	uint64_t length = 0;

	struct pollfd pfd[1];
	pfd[0].fd = dvr_fd;
	pfd[0].events = POLLIN;

  int c;
  set_conio_terminal_mode();
  do {
	if (poll(pfd,1,1))
	{
		if (pfd[0].revents & POLLIN)
		{
			len = read(dvr_fd, buf, BUFFY);
			if (len > 0)
			{
				if (len < 500000)
						stime += 5000;
				else
						stime -= 20000;
				printf ("stime: %d len: %d ", stime, len);
				if (stime)
					usleep(stime);

				length += fwrite(buf, 1, len, dvr_output);
				printf("Written %2.0f MB ...\r", length/1024./1024.);
			} else {
				perror("recording");
				return -1;
			}
		}
	}
	if ( kbhit() )
  		c = kbgetchar(); /* consume the character */
  } while( (char)c != 'q' );
}

char *usage =
	"\n"
	"  usage: supply a vpid, apid, pcr, and pmt pid to create a vlc playable ts file \n"
	"  	demux [options] -pids 1111 2222 333 444 \n"
	"  	-adapter N     : use given adapter (default 0) \n"
	"  	-filename      : use filename, default is /tmp/test.ts \n"
	"  	-mode          : decoder, file, or dvr (default file) \n"
	"  	-pids          : use to supply the app with the list of pids you want saved \n" 
	"  	-daemon N      : 0 = normal, 1 = daemonize (default 0) \n"
	"  	-help          : help\n\n";

int main(int argc, char *argv[])
{
	if (argc > 1 && strcmp(argv[1], "-help") == 0)
	{
		printf("%s", usage);
		return -1;
	}

	char demux_devname[80];
	char dvr_devname[80];
	int daemon = 0;
	char decoder_devname[80] = "/dev/dvb/adapter0/video0";
	char *dvr_filename = "/tmp/test.ts";
	int adapter = 0;
	char *mode = "file";
	int pids[20];
	int num_pids = 0;

	int a, p;
	for( a = 1; a < argc; a++ )
	{
		if ( !strcmp(argv[a], "-daemon") )
                        daemon = strtoul(argv[a + 1], NULL, 0);
		if ( !strcmp(argv[a], "-adapter") )
			adapter = strtoul(argv[a+1], NULL, 0);
		if ( !strcmp(argv[a], "-filename") )
			dvr_filename = argv[a+1];
		if ( !strcmp(argv[a], "-mode") )
			mode = argv[a+1];
		if ( !strcmp(argv[a], "-help") )
		{
			printf("%s", usage);
			return -1;
		}
		if ( !strcmp(argv[a], "-pids") )
			for( p = a+1 ; p < argc; p++ )
			{
				pids[num_pids] = strtoul(argv[p], NULL, 0);
				num_pids++;
			}
	}
	if (!num_pids)
	{
		num_pids++;
		pids[0] = 8192;
	}

	snprintf(dvr_devname, 80, "/dev/dvb/adapter%d/dvr0", adapter);
	snprintf(demux_devname, 80, "/dev/dvb/adapter%d/demux0", adapter);
	if(daemon == 1)
	{
		printf("daemonize -> yes (%d), don't forget to kill it when finished demuxing\n", daemon);
		daemonize();
	}
	else	printf("daemonize -> no (%d)\n", daemon);
		
	int demux_fd[20];
	struct dmx_pes_filter_params pesFilterParams;
	for( p = 0 ; p < num_pids; p++ )
	{
		printf("Opening: %s PID #%d %04d (0x%04x) \n", demux_devname, p, pids[p], pids[p]);
		if ((demux_fd[p] = open(demux_devname, O_RDWR|O_NONBLOCK)) < 0)
		{
			printf("failed to open demux '%s': %d %m\n", demux_devname, errno);
			return -1;
		}

		ioctl(demux_fd[p], DMX_SET_BUFFER_SIZE, BUFFY);
		pesFilterParams.pid = pids[p];
		pesFilterParams.input = DMX_IN_FRONTEND; 
		pesFilterParams.output = !strcmp(mode, "decoder") ? DMX_OUT_TAP : DMX_OUT_TS_TAP;
		pesFilterParams.pes_type = DMX_PES_OTHER; 
		pesFilterParams.flags = DMX_IMMEDIATE_START;
		if (ioctl(demux_fd[p], DMX_SET_PES_FILTER, &pesFilterParams) == -1){
			perror("DEMUX: DMX_SET_PES_FILTER");
			return -1; 
		}
	}

	if (!strcmp(mode, "dvr"))
	{
		int c;
		do {
		if ( kbhit() )
			c = kbgetchar(); /* consume the character */
			usleep(100000);
		} while( (char)c != 'q' );
	}
	if (!strcmp(mode, "decoder"))
	{
		printf("Opening: %s\n", dvr_devname);
		int dvr_fd;
		if ((dvr_fd = open(dvr_devname, O_RDONLY|O_NONBLOCK)) < 0)
		{
			printf("failed to open '%s': %d %m\n", dvr_devname, errno);
			return -1;
		}
		printf("Opening: %s\n", decoder_devname);
		int decoder_fd;
		if ((decoder_fd = open(decoder_devname, O_RDWR)) < 0){
			printf("failed to open '%s': %d %m\n", decoder_devname, errno);
			return -1;
		}

		sendtodecoder(demux_fd[0], decoder_fd);
		close(decoder_fd);

		printf("Closing demux ... \n");
		close(dvr_fd);
	}
	if (!strcmp(mode, "file"))
	{
		printf("Opening: %s\n", dvr_devname);
		int dvr_fd;
		if ((dvr_fd = open(dvr_devname, O_RDONLY|O_NONBLOCK)) < 0)
		{
			printf("failed to open '%s': %d %m\n", dvr_devname, errno);
			return -1;
		}
		printf("Opening: %s\n", dvr_filename);
		FILE * dvr_output;
		if ((dvr_output = fopen(dvr_filename, "w")) < 0){
			printf("failed to open '%s': %d %m\n", dvr_filename, errno);
			return -1;
		}

		sendtofile(dvr_fd, dvr_output);
		fflush(dvr_output);
		fclose(dvr_output);

		printf("Closing demux ... \n");
		close(dvr_fd);
	}
	for( p = 0 ; p < num_pids; p++ )
		close(demux_fd[p]);
	return 0;
}
